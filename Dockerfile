FROM alpine:edge

# Puppeteer
# See https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md
# - Use installed chromium 76 instead of downloading new one
# - Use puppeteer 1.17.0 which works with chromium 76
# - Use --no-sandbox to use puppeteer as root and specify the path of installed chromium 76
# {
#   "args":["--no-sandbox"],
#   "executablePath":"/usr/bin/chromium-browser"
# }

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

RUN rm -rf /var/cache/apk/* /tmp/* && \
    echo https://conoria.gitlab.io/alpine-pandoc/ >> /etc/apk/repositories && \
    apk update --allow-untrusted && \
    apk add \
      --allow-untrusted \
      --no-cache \
      --upgrade \
      bash \
      grep \
      git \
      gcc \
      musl-dev \
      go \
      rsync \
      wget \
      texlive \
      texlive-xetex \
      texlive-luatex \
      texmf-dist-latexextra \
      chromium \
      nss \
      freetype \
      freetype-dev \
      harfbuzz \
      ca-certificates \
      ttf-freefont \
      nodejs \
      npm \
      python3 \
      librsvg \
      pandoc \
      pandoc-citeproc && \
    mkdir -p /usr/share/fonts/ && \
    cd /usr/share/fonts/ && \
    wget http://cdn.naver.com/naver/NanumFont/fontfiles/NanumFont_TTF_ALL.zip && \
    mkdir NanumFont && \
    unzip NanumFont_TTF_ALL.zip -d NanumFont && \
    fc-cache -fv && \
    npm install -g puppeteer@1.17.0 mermaid-filter --unsafe-perm=true --allow-root && \
    rm -rf /var/cache/apk/* /tmp/* && \
    wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 && \
    chmod +x /usr/local/bin/dumb-init && \
    ln -s /usr/bin/python3 /usr/bin/python && \
    pip3 install pandocfilters && \
    wget -O /usr/local/bin/svg-filter https://gist.githubusercontent.com/kylebarron/576b47148e162f939bf722778cfdc97a/raw/a4caeae49f978ef259dea4d5ef08ebc17487a11a/pandoc-svg.py && \
    chmod +x /usr/local/bin/svg-filter

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
