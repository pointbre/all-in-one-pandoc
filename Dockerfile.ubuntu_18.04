FROM ubuntu:19.04

# Non-interactive mode
ARG DEBIAN_FRONTEND=noninteractive
ARG PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig/

# Install texlive-full including xetex
RUN rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    apt-get update && \
    apt-get -y install \
      texlive-full \
      texlive-xetex && \
    apt-get purge -f -y \
      texlive-fonts-extra-doc \
      texlive-fonts-recommended-doc \
      texlive-humanities-doc \
      texlive-latex-base-doc \
      texlive-latex-extra-doc \
      texlive-latex-recommended-doc \
      texlive-metapost-doc \
      texlive-pictures-doc \
      texlive-pstricks-doc \
      texlive-publishers-doc \
      texlive-science-doc && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

# Install pandoc, nanum font, google chrome and mermaid-filter
RUN rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    apt-get update && \
    apt-get install -y \
      apt-utils \
      gnupg2 \
      wget \
      curl && \
    wget https://github.com/jgm/pandoc/releases/download/2.7.3/pandoc-2.7.3-1-amd64.deb && \
    dpkg -i pandoc-2.7.3-1-amd64.deb && \
    apt-get install -f -y && \
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list && \
    curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get update && \
    apt-get install -y \
      fonts-nanum \
      google-chrome-stable \
      nodejs && \
    npm install -g mermaid-filter --unsafe-perm=true --allow-root && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

# Install dumb-init and set it as entry point
RUN wget https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64.deb
RUN dpkg -i dumb-init_*.deb
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
